(function () {
  'use strict';

  angular
    .module('ead')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, webDevTec, $mdSidenav, $log, $scope, $mdUtil, $rootScope, $state) {

    $scope.distribution = function () {
      $state.go('home.distribution');
    }
    $scope.finance = function () {
      $state.go('home.finance');
    }

    $scope.hr = function () {
      $state.go('home.hr');
    }
    $scope.manufacture = function () {
      $state.go('home.manufacture');
    }
    $scope.sales= function () {
      $state.go('home.sales');
    }



  }

})();

