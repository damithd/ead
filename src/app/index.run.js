(function() {
  'use strict';

  angular
    .module('ead')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
