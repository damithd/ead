/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('ead')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
