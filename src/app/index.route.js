(function() {
  'use strict';

  angular
    .module('ead')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
  .state('home.manufacture', {
      url: 'manufacture',
      views: {
        'main': {
          templateUrl: 'app/components/manufacture/manufacture.html',
          controller: 'ManufactureController',
          controllerAs: 'manufacture'
        }
      }
    })
      .state('home.distribution', {
        url: 'distribution',
        views: {
          'main': {
            templateUrl: 'app/components/distribution/distribution.html',
            controller: 'DistributionController',
            controllerAs: 'distribution'
          }
        }
      })
      .state('home.hr', {
        url: 'hr',
        views: {
          'main': {
            templateUrl: 'app/components/hr/hr.html',
            controller: 'HrController',
            controllerAs: 'hr'
          }
        }
      })
      .state('home.finance', {
        url: 'finance',
        views: {
          'main': {
            templateUrl: 'app/components/finance/finance.html',
            controller: 'FinanceController',
            controllerAs: 'finance'
          }
        }
      })
      .state('home.sales', {
        url: 'sales',
        views: {
          'main': {
            templateUrl: 'app/components/sales/sales.html',
            controller: 'SalesController',
            controllerAs: 'sales'
          }
        }
      })


    ;

    $urlRouterProvider.otherwise('/');
  }

})();
